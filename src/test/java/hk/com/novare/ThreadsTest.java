package hk.com.novare;

import hk.com.novare.threads.ChildrenThread;
import hk.com.novare.threads.CustomThread;
import org.junit.jupiter.api.Test;

public class ThreadsTest {
    @Test
    public void currentThread() throws InterruptedException {
        System.out.println(
            Thread.currentThread().getName()
        );
        Thread.sleep(1000);
        System.out.println(
            Thread.currentThread().getId()
        );
        System.out.println(
            Thread.currentThread().getPriority()
        );
        System.out.println(
            Thread.currentThread().isDaemon()
        );
    }

    @Test
    public void childThreadAsync() {
        final Thread child = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(1000);
                    System.out.println("I am a child thread");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
        child.start();
        System.out.println("Main thread done!");
    }

    @Test
    public void childThreadJoin() throws InterruptedException {
        final Thread child = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(1000);
                    System.out.println("I am an another child thread");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
        System.out.println("Starting the child");
        child.start();
        child.join();
        System.out.println("Main thread done!");
    }

    @Test
    public void customThreadSingle() throws InterruptedException {
        final CustomThread thread = new CustomThread(5000, "I am here!");
        thread.start();
        thread.join();
    }


    @Test
    public void customThreadObject() throws InterruptedException {
        final CustomThread thread1 = new CustomThread(1000, "hello");
        final CustomThread thread2 = new CustomThread(2000, "bye");
        final CustomThread thread3 = new CustomThread(1500, "welcome");

        thread3.start();
        thread3.join();

        thread2.start();
        thread1.start();

        thread2.join();
        thread1.join();
    }

    @Test
    public void nestedThreads() throws InterruptedException {
        final ChildrenThread child1 = new ChildrenThread("Threey", 3);
        final ChildrenThread child2 = new ChildrenThread("Fourey", 4);

        child1.start();
        child2.start();

        child1.join();
        child2.join();
    }
}
