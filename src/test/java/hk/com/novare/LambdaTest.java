package hk.com.novare;

import org.junit.jupiter.api.Test;

import java.util.*;
import java.util.stream.Collectors;

public class LambdaTest {
    @Test
    public void noLambda() {
        final String output = new StringTransformer(
            "Lambda",
            new LowerCase()
        ).apply();
        System.out.println(output);
    }

    @Test
    public void withLambda() {
        final String output = new StringTransformer(
            "Lambda",
            input -> input.toLowerCase()
        ).apply();
        System.out.println(output);
    }

    @Test
    public void withLambdaMethodReference() {
        final String output = new StringTransformer(
            "Lambda",
            String::toLowerCase
        ).apply();
        System.out.println(output);
    }

    @Test
    public void streamExample() {
        final List<String> strings = new ArrayList<>();
        strings.add("Bryan");
        strings.add("Kurt");

        // For each version
        for (final String s : strings) {
            System.out.println(s);
        }

        // Stream version
        strings
            .forEach(System.out::println);
    }

    @Test
    public void streamExampleWithTransformation() {
        final List<Number> numbers = new ArrayList<>();
        numbers.add(1);
        numbers.add(2.0);
        numbers.add(3f);
        numbers.add(3L);

        // Stream version
        numbers
            .stream()
            .map(LambdaTest::describeNumber)
            .map(String::toUpperCase)
            .forEach(System.out::println);
    }

    @Test
    public void streamWithNullHandlingPrimitive() {
        List<String> filenames = Arrays.asList(
            "tutorial1.mp4",
            "tutorial2.mp4",
            "tutorial3.mp4",
            null,
            "tutorial5.mp4",
            "tutorial5.mp4"
        );

        filenames
            .stream()
            .filter(Objects::nonNull)
            .map(filename -> {
                if (filename != null) {
                    return filename.split("\\.")[0];
                }
                return null;
            })
            .forEach(System.out::println);
    }

    @Test
    public void streamNoNullHandling() {
        List<String> filenames = Arrays.asList(
            "tutorial1.mp4",
            "tutorial2.mp4",
            "tutorial3.mp4",
            null,
            "tutorial5.mp4",
            "tutorial5.mp4"
        );

        filenames
            .stream()
            .map(filename -> filename.split("\\.")[0])
            .forEach(System.out::println);
    }

    @Test
    public void optionalPresent() {
        System.out.println(
            Optional.of("Jed").isPresent()
        );
        System.out.println(
            Optional.empty().isPresent()
        );
        System.out.println(
            Optional.ofNullable(null).isPresent()
        );
    }

    @Test
    public void optionalGet() {
        System.out.println(
            Optional.of("Jed").get()
        );
//        System.out.println(
//            Optional.empty().get()
//        );
//        System.out.println(
//            Optional.ofNullable(null).get()
//        );
        System.out.println(
            Optional.ofNullable(null).equals(Optional.empty())
        );
    }

    @Test
    public void streamWithNullHandling() {
        List<String> filenames = Arrays.asList(
            "tutorial1.mp4",
            "tutorial2.mp4",
            "tutorial3.mp4",
            null,
            "tutorial5.mp4",
            "tutorial5.mp4"
        );

        filenames
            .stream()
            // maps a value to another value
            .map(Optional::ofNullable)
            // omits a value that falsifies the predicate
            .filter(Optional::isPresent)
            .map(Optional::get)
            .map(filename -> filename.split("\\.")[0])
            .forEach(System.out::println);
    }

    @Test
    public void streamWithNullHandlingParallel() {
        List<String> filenames = Arrays.asList(
            "tutorial1.mp4",
            "tutorial2.mp4",
            "tutorial3.mp4",
            null,
            "tutorial5.mp4",
            "tutorial6.mp4"
        );

        filenames
            .parallelStream()
            // maps a value to another value
            .map(Optional::ofNullable)
            // omits a value that falsifies the predicate
            .filter(Optional::isPresent)
            .map(Optional::get)
            .map(filename -> filename.split("\\.")[0])
            .forEach(System.out::println);
    }

    @Test
    public void streamThenCollect() {
        List<String> filenames = Arrays.asList(
            "tutorial1.mp4",
            "tutorial2.mp4",
            "tutorial3.mp4",
            null,
            "tutorial5.mp4",
            "tutorial5.mp4"
        );

        final List<String> cleanFilenames = filenames
            .stream()
            .map(Optional::ofNullable)
            .filter(Optional::isPresent)
            .map(Optional::get)
            .map(filename -> filename.split("\\.")[0])
            // Collects the stream back to a list
            .collect(Collectors.toList());

        System.out.println(cleanFilenames);
    }

    @Test
    public void streamThenCollectGroup() {
        List<String> filenames = Arrays.asList(
            "tutorial1.mp4",
            "tutorial2.mp4",
            "tutorial3.mp3",
            "tutorial5.mp4",
            "tutorial6.mp3"
        );

        Map<String, List<String>> grouped =filenames
            .stream()
            .map(Optional::ofNullable)
            .filter(Optional::isPresent)
            .map(Optional::get)
            .collect(Collectors.groupingBy(
                filename -> filename.split("\\.")[1])
            );

        System.out.println(grouped);
    }

    private static String describeNumber(final Number number) {
        String output = String.valueOf(number);
        final String type = number.getClass().getName();
        return output + " is of type " + type;
    }
}

class StringTransformer {
    private final String input;
    private final StringChange change;

    StringTransformer(final String input, final StringChange change) {
        this.input = input;
        this.change = change;
    }

    public String apply() {
        return this.change.doChange(
            this.input
        );
    }
}

class LowerCase implements StringChange {
    @Override
    public String doChange(final String input) {
        return input.toLowerCase();
    }
}
interface StringChange {
    String doChange(String input);
}
