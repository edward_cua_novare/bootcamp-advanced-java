package hk.com.novare;

import hk.com.novare.generics.*;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.util.*;

public class GenericsTest {
    @Test
    public void basicGenericsInteger() {
        final BasicGenerics<Integer> basic = new BasicGenerics<>(1);
        System.out.println(
            basic.get()
        );
    }

    @Test
    public void basicGenericsDouble() {
        final BasicGenerics<Double> basic = new BasicGenerics<>(1.0);
        System.out.println(
            basic.get()
        );
    }

    @Test
    public void basicGenericsString() {
        final BasicGenerics<String> basic = new BasicGenerics<>("Hello World");
        System.out.println(
            basic.get()
        );
    }

    @Test
    public void basicGenericsOfGenerics() {
        final BasicGenerics<BasicGenerics<String>> basic = new BasicGenerics<>(
            new BasicGenerics<>("Inception")
        );
        System.out.println(
            basic.get().get()
        );
    }

    @Test
    public void compoundGenerics() {
        final CompoundGenerics<Double, String> compound = new CompoundGenerics<>(1.0, "One");
        System.out.println(
            compound.getFirst()
        );
        System.out.println(
            compound.getSecond()
        );
    }

    @Test
    public void builtInGenericsList() {
        final List<String> names = new ArrayList<>();
        names.add("Jed");
        names.add("John");
        names.add("Java");

        for (final String name : names) {
            System.out.println(name);
        }
    }

    @Test
    public void builtInGenericsListCombined() {
        final List<BasicGenerics<Integer>> generics = new LinkedList<>();
        generics.add(new BasicGenerics<>(1));
        generics.add(new BasicGenerics<>(3));
        generics.add(new BasicGenerics<>(2));

        for (final BasicGenerics<Integer> generic : generics) {
            System.out.println(
                generic.get()
            );
        }
    }

    @Test
    public void builtInGenericsMap() {
        final Map<String, Integer> directory = new TreeMap<>();
        directory.put("Jed", 1);
        directory.put("John", 2);
        directory.put("Java", 3);

        for (final String name : directory.keySet()) {
            System.out.println(
                name + " => " + directory.get(name)
            );
        }
    }

    @Test
    public void builtInGenericsOptionalExample() {
        final String userInput = "Jed";

        if (userInput != null) {
            System.out.println(userInput.toUpperCase().substring(2));
        } else {
            System.out.println("No input");
        }
    }

    @Test
    public void builtInGenericsOptional() {
        final Optional<String> userInput = Optional.of("Jed");
        System.out.println(
            userInput
                .map(name -> name.toUpperCase())
                .orElse("No input")
        );
    }

    @Test
    public void example() {
        final Report timesheet = new Timesheet(LocalDate.of(2019, 7, 22));
        final Report timesheet2 = new Timesheet(LocalDate.now());
        final Report payslip = new Payslip(LocalDate.now(), 10);
        final Report payslip2 = new Payslip(LocalDate.of(2019, 7, 14), 10);

        final String summary = new Summary<>(
            new Report[]{
                timesheet,
                payslip,
                payslip2,
                timesheet2
            }
        ).summarize();

        System.out.println(summary);
    }
}
