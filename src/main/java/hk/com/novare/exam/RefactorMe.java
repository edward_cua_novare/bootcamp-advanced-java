package hk.com.novare.exam;

import java.util.Random;

public final class RefactorMe {
    public void run() {
        final Random random = new Random();

        String integers = "";
        // Generate 1000 random integers
        for (int i = 0; i < 1000; i++) {
            integers += random.nextInt(100) + ",";
        }
        System.out.println("The numbers are: " + integers);

        // Calculate the sum of the numbers
        Integer sum = 0;
        for (final String integer : integers.split(",")) {
            sum += Integer.valueOf(integer);
        }
        System.out.println("The sum is: " + sum);

        // Print all the even numbers
        String evens = "";
        for (final String integer : integers.split(",")) {
            Integer thisInt = Integer.valueOf(integer);
            if (thisInt % 2 == 0) {
                evens += integer + ",";
            }
        }
        System.out.println("The evens are: " + evens);
    }
}
