package hk.com.novare.threads;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public final class ChildrenThread extends Thread {
    private final String name;
    private final Integer children;

    public ChildrenThread(final String name, final Integer children) {
        this.name = name;
        this.children = children;
    }

    @Override
    public void run() {
        List<Thread> threads = new ArrayList<>();
        final Random random = new Random();
        for (int i = 0; i < children; i++) {
            final CustomThread thread = new CustomThread(
                random.nextInt(5000),
                "I am a " + i + " child of " + name
            );
            thread.start();
            threads.add(thread);
        }
        for (final Thread thread : threads) {
            try {
                thread.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
