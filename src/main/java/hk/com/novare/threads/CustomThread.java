package hk.com.novare.threads;

public final class CustomThread extends Thread {
    private final Integer delay;
    private final String output;

    public CustomThread(final Integer delay, final String output) {
        this.delay = delay;
        this.output = output;
    }

    @Override
    public void run() {
        try {
            Thread.sleep(this.delay);
            System.out.println(this.output);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
