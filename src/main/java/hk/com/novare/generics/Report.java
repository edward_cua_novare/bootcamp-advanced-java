package hk.com.novare.generics;

import java.time.LocalDate;

public interface Report {
    LocalDate getDateFinished();
}
