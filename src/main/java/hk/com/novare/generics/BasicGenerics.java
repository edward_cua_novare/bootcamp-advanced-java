package hk.com.novare.generics;

/**
 * Basic generic class. Notice the 'T' representing any type
 * @param <T> type
 */
public final class BasicGenerics<T> {
    private final T t;

    public BasicGenerics(final T t) {
        this.t = t;
    }

    public T get() {
        return this.t;
    }
}
