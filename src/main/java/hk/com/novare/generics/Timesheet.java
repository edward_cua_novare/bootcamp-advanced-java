package hk.com.novare.generics;

import java.time.LocalDate;

public final class Timesheet implements Report {
    private final LocalDate date;

    public Timesheet(final LocalDate date) {
        this.date = date;
    }

    @Override
    public LocalDate getDateFinished() {
        return this.date;
    }
}
