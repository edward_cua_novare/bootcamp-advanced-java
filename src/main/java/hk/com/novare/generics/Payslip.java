package hk.com.novare.generics;

import java.time.LocalDate;

public final class Payslip implements Report {
    private final LocalDate startDate;
    private final Integer days;

    public Payslip(final LocalDate startDate, final Integer days) {
        this.startDate = startDate;
        this.days = days;
    }

    @Override
    public LocalDate getDateFinished() {
        return this.startDate.plusDays(this.days);
    }
}
