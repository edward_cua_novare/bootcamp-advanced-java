package hk.com.novare.generics;

/**
 * Generics with 2 types.
 * Notice it can be any letter not just 'T'
 * @param <X> 1st type
 * @param <Y> 2nd type
 */
public final class CompoundGenerics<X extends Number, Y> {
    private final X x;
    private final Y y;

    public CompoundGenerics(final X x, final Y y) {
        this.x = x;
        this.y = y;
    }

    public X getFirst() {
        return this.x;
    }

    public Y getSecond() {
        return this.y;
    }
}
