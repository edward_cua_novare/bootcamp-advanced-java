package hk.com.novare.generics;

import java.time.LocalDate;

public final class Summary<R extends Report> {
    private final R[] reports;

    public Summary(final R[] reports) {
        this.reports = reports;
    }

    public String summarize() {
        LocalDate lowest = null;
        LocalDate highest = null;

        for (final R r : this.reports) {
            if (lowest == null) {
                lowest = r.getDateFinished();
            }

            if (highest == null) {
                highest = r.getDateFinished();
            }

            LocalDate current = r.getDateFinished();
            if (lowest.isAfter(current)) {
                lowest = current;
            }

            if (highest.isBefore(current)) {
                highest = current;
            }
        }

        return String.format(
            "Lowest date is %s, Highest date is %s",
            lowest,
            highest
        );
    }
}
