package hk.com.novare;

import hk.com.novare.exam.RefactorMe;

public final class Main {
    public static void main(final String... args) {
        final RefactorMe refactor = new RefactorMe();
        refactor.run();
    }
}
